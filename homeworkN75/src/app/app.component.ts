import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CodeService } from '../code-service';
import { Code, Coding, Uncode } from '../code.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @ViewChild('f') form!: NgForm;

  constructor(private codeService: CodeService) {
  }

  coding() {
    const code: Code = {
      word: this.form.value.encode,
      password: this.form.value.password,
    };
    this.codeService.postCode(code).subscribe(encode => {
      this.setFormValue({
        decode: (<Coding>encode).encode,
        encode: '',
        password: this.form.value.password,
      });
    });
  };

  decoding() {
    const decode: Code = {
      word: this.form.value.decode,
      password: this.form.value.password,
    };
    this.codeService.postDecode(decode).subscribe(encode => {
      this.setFormValue({
        decode: this.form.value.encode,
        encode: (<Uncode>encode).decode,
        password: this.form.value.password,
      });
    });
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.form.setValue(value);
    });
  }
}
