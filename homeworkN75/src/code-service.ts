import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Code } from './code.model';

@Injectable({
  providedIn: 'root'
})

export class CodeService {
  constructor(private http: HttpClient){}

  postCode(code: Code){
    console.log(code);
    return this.http.post('http://localhost:8080/encode', code)
  }

  postDecode(code: Code){
    return this.http.post('http://localhost:8080/decode', code)
  }

}
