export class Code {
  constructor(
    public word: string,
    public password: string,
  ){}
}
export class Uncode {
  constructor(
    public decode: string
  ) {}
}
export class Coding {
  constructor(
    public encode: string
  ) {}
}
