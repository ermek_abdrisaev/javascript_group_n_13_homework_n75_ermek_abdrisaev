const express = require('express');
const cors = require('cors');
const app = express();
const port = 8080;

const Vigenere = require('caesar-salad').Vigenere;

app.use(express.json());
app.use(cors({origin: 'http://localhost:4200'}));

app.post('/encode', (req, res) => {
  const encode = {
    encode: Vigenere.Cipher(req.body.password).crypt(req.body.word)
  }
  return res.send(encode);
});

app.post('/decode', (req, res) => {
  const decode = {
    decode: Vigenere.Decipher(req.body.password).crypt(req.body.word)
  }
  return res.send(decode);
});

app.listen(port, () => {
  console.log('You are on' + port);
});